
class Figure:

    def __init__(self, field):
        self.field = field

    def list_available_moves(self):
        pass

    def validate_move(self, dest_field):
        moves = self.list_available_moves()
        for move in moves:
            if move == dest_field:
                return True
        return False


class Pawn(Figure):

    def list_available_moves(self):
        moves = []
        if int(self.field[1]) == 1:
            moves.append(self.field[0] + str(2))
            moves.append(self.field[0] + str(3))
        else:
            moves.append(self.field[0] + str(int(self.field[1]) + 1))
        return moves


class Knight(Figure):

    def list_available_moves(self):
        x_cross = [-2, -1, 1, 2, -2, -1, 1, 2]
        y_cross = [-1, -2, -2, -1, 1, 2, 2, 1]
        moves = []
        x = int(ord(self.field[0])) - 64
        y = int(self.field[1])

        for i in range(0, 8):
            tmp_x = x
            tmp_y = y
            tmp_x = tmp_x + x_cross[i]
            tmp_y = tmp_y + y_cross[i]
            if tmp_x < 1 or tmp_x > 8 or tmp_y < 1 or tmp_y > 8:
                continue
            else:
                moves.append(chr(tmp_x + 64) + str(tmp_y))

        for i in moves:
            if(i == self.field):
                moves.remove(i)
        return moves


class Bishop(Figure):

    def list_available_moves(self):

        x_cross = [-1, 1, -1, 1]
        y_cross = [-1, 1, 1, -1]

        moves = []
        x = int(ord(self.field[0])) - 64
        y = int(self.field[1])

        for i in range(0, 4):
            tmp_x = x
            tmp_y = y
            while True:
                tmp_x = tmp_x + x_cross[i]
                tmp_y = tmp_y + y_cross[i]
                if tmp_x < 1 or tmp_x > 8 or tmp_y < 1 or tmp_y > 8:
                    break
                else:
                    moves.append(chr(tmp_x + 64) + str(tmp_y))

        for i in moves:
            if(i == self.field):
                moves.remove(i)
        return moves


class Rook(Figure):

    def list_available_moves(self):
        moves = []

        for i in range(0, 8):
            moves.append(chr(65 + i) + str(self.field[1]))

        for i in range(1, 9):
            moves.append(self.field[0] + str(i))

        for i in moves:
            if(i == self.field):
                moves.remove(i)

        return moves


class Queen(Figure):

    def list_available_moves(self):

        x_cross = [-1, 1, -1, 1]
        y_cross = [-1, 1, 1, -1]

        moves = []
        x = int(ord(self.field[0])) - 64
        y = int(self.field[1])
        for i in range(0, 8):
            moves.append(chr(65 + i) + str(self.field[1]))

        for i in range(1, 9):
            moves.append(self.field[0] + str(i))

        for i in range(0, 4):
            tmp_x = x
            tmp_y = y
            while True:
                tmp_x = tmp_x + x_cross[i]
                tmp_y = tmp_y + y_cross[i]
                if tmp_x < 1 or tmp_x > 8 or tmp_y < 1 or tmp_y > 8:
                    break
                else:
                    moves.append(chr(tmp_x + 64) + str(tmp_y))

        for i in moves:
            if(i == self.field):
                moves.remove(i)
        return moves


class King(Figure):

    def list_available_moves(self):
        moves = []
        for i in range(0, 3):
            for j in range(0, 3):
                x = int(ord(self.field[0])) + i - 1
                y = int(self.field[1]) + j - 1
                if (x >= 65 and x <= 72) and (y >= 1 and y <= 8):
                    moves.append(chr(x) + str(y))
        moves.remove(self.field)
        return moves


class FigureFactory:

    @staticmethod
    def create(name, field):
        name = name.lower()
        if(name == 'pawn'):
            return Pawn(field)
        if(name == 'knight'):
            return Knight(field)
        if(name == 'bishop'):
            return Bishop(field)
        if(name == 'rook'):
            return Rook(field)
        if(name == 'queen'):
            return Queen(field)
        if(name == 'king'):
            return King(field)
        return None
