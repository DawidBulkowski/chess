import flask
import models
import models_api
from flask import jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True
    

@app.route('/api/v1/<figure>/<field>', methods=['GET'])
def get_available_moves(figure, field):
    obj = models.FigureFactory.create(figure, field)
    error = ''
    moves = []
    code = 200

    if not obj:
        error = 'Figure does not exist'
        code = 404

    if len(field) != 2 or field[0] < 'A' or field[0] > 'H' or int(field[1]) < 1 or int(field[1]) > 8:
        error = 'Field does not exist'
        code = 409

    if code == 200:
        moves = obj.list_available_moves()

    return jsonify(models_api.AvailableModel(moves, error, figure, field).get_data()), code


@app.route('/api/v1/<figure>/<field>/<dest>', methods=['GET'])
def check_field(figure, field, dest):

    obj = models.FigureFactory.create(figure, field)
    error = ''
    correct = False
    code = 200

    if not obj:
        error = 'Figure does not exist'
        code = 404

    if len(field) != 2 or field[0] < 'A' or field[0] > 'H' or int(field[1]) < 1 or int(field[1]) > 8 or len(dest) != 2 or dest[0] < 'A' or dest[0] > 'H' or int(dest[1]) < 1 or int(dest[1]) > 8:
        error = 'Field does not exist'
        code = 409

    if code == 200:
        correct = obj.validate_move(dest)
        if not correct:
            error = 'Current move is not permitted.'

    return jsonify(models_api.CorrectField(correct, error, figure, field, dest).get_data()), code


app.run()
