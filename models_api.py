
class AvailableModel:

    availableMoves = []
    error = None
    figure = ''
    currentField = ''

    def __init__(self, availableMoves, error, figure, currentField):
        self.availableMoves = availableMoves
        self.error = error
        self.figure = figure
        self.currentField = currentField

    def get_data(self):
        data = {
            'availableMoves': self.availableMoves,
            'error': self.error,
            'figure': self.figure,
            'currentField': self.currentField
        }
        return data


class CorrectField:

    move = False
    error = None
    figure = ''
    currentField = ''
    destField = ''

    def __init__(self, move, error, figure, currentField, destField):
        self.move = move
        self.error = error
        self.figure = figure
        self.currentField = currentField
        self.destField = destField

    def get_data(self):

        if self.move:
            move = 'valid'
        else:
            move = 'invalid'
        data = {
            'move': move,
            'error': self.error,
            'figure': self.figure,
            'currentField': self.currentField,
            'destField': self.destField
        }
        return data
