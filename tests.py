import random, unittest, requests, ipdb
from models import King, Pawn, Queen, Bishop, Rook, Knight


def test_kingA5CorrectMoves():
    king_obj = King('A5')
    king_function_moves = king_obj.list_available_moves()
    correct_moves = ['A4', 'A6', 'B4', 'B5', 'B6']
    assert len(king_function_moves) == len(correct_moves)
    assert sorted(king_function_moves) == sorted(correct_moves)

def test_pawnA5CorrectMoves():
    pawn_obj = Pawn('A5')
    pawn_function_moves = pawn_obj.list_available_moves()
    correct_moves = ['A6']
    assert len(pawn_function_moves) == len(correct_moves)
    assert sorted(pawn_function_moves) == sorted(correct_moves)

def test_queenA1CorrectMoves():
    queen_obj = Queen('A1')
    queen_function_moves = queen_obj.list_available_moves()
    correct_moves = ['A2','A3','A4','A5','A6','A7','A8','B2','C3','D4','E5','F6','G7','H8','B1','C1','D1','E1','F1','G1','H1']
    assert len(queen_function_moves) == len(correct_moves)
    assert sorted(queen_function_moves) == sorted(correct_moves)
  
def test_bishopD4CorrectMoves():
    bishop_obj = Bishop('D4')
    bishop_function_moves = bishop_obj.list_available_moves()
    correct_moves = ['A7','B6','C5','E3','F2','G1','A1','B2','C3','E5','F6','G7','H8']
    assert len(bishop_function_moves) == len(correct_moves)
    assert sorted(bishop_function_moves) == sorted(correct_moves)   

def test_rookA1CorrectMoves():
    rook_obj = Rook('A1')
    rook_function_moves = rook_obj.list_available_moves()
    correct_moves = ['A2','A3','A4','A5','A6','A7','A8','B1','C1','D1','E1','F1','G1','H1']
    assert len(rook_function_moves) == len(correct_moves)
    assert sorted(rook_function_moves) == sorted(correct_moves)

def test_knightH1CorrectMoves():
    knight_obj = Knight('H1')
    knight_function_moves = knight_obj.list_available_moves()
    correct_moves = ['F2','G3']
    assert len(knight_function_moves) == len(correct_moves)
    assert sorted(knight_function_moves) == sorted(correct_moves)      